<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

/**
 * Class XCBundle
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
namespace XDev\Module\XCBundle;

class Module extends \XDev\Base\ABundleModule
{
    public function getModuleDescription()
    {
        return 'X-Cart Bundle';
    }
}
